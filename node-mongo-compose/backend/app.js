const express = require('express')
const restful = require('node-restful')
const server = express()
const mongoose = restful.mongoose
const bodyParser = require('body-parser')
const cors = require('cors')

//Database - faz a conexão com o mongdb
mongoose.Promise = global.Promise //O Promise do mongoose está decrepted, por isso a associação ao o do global
mongoose.connect('mongodb://db/myDb') //db é o nome do servidor e myDb é o nome do schema. Será configurado no compoose

//Middlewares
server.use(bodyParser.urlencoded({extended:true}))
server.use(bodyParser.json())
server.use(cors())

//ODM - Mapeamento Objeto-Documento
const Client = restful.model('Client', {
    name: { type: String, require: true}
})

//Rest Api
Client.methods(['get', 'post', 'put', 'delete'])
Client.updateOptions({new: true, runValidators: true})

//Rotas
Client.register(server, '/clients')

//Starta o Serviço na porta 3000
server.listen(3000)
